/*
 * Copyright (C) 2022  Comiryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * putaside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import QtQuick.LocalStorage 2.0
import "js/Database.js" as MyDB

MainView {

    id: root
    objectName: 'mainView'
    applicationName: 'putaside.comiryu'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    Settings {
        id: settings

        property bool firstStart: true

        // Theme settings
        readonly property string accentColor: "#c74375"
        readonly property string backgroundColor: "#333333"
        readonly property string foregroundColor: "#ffffff"

        // Backend Settings
        readonly property int margin: units.gu(2)
    }

    Rectangle {
      height: parent.height
      width: parent.width

      color: settings.backgroundColor
    }

    PageStack {
      id: pageStack
      Component.onCompleted: pageStack.push(Qt.resolvedUrl("./Pages/Savings.qml"))
    }
}

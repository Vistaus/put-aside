function openDatabase() {
  var database = LocalStorage.openDatabaseSync("SavingsDatabase", "1.0", "Put aside database for savings", 1000);
  try {
    database.transaction(function(tx) {
        tx.executeSql('CREATE TABLE IF NOT EXISTS savings(id INTEGER PRIMARY KEY, title TEXT, notes TEXT, url TEXT, amount DOUBLE)');
        tx.executeSql('CREATE TABLE IF NOT EXISTS savingsHistory(id INTEGER PRIMARY KEY, savings_id INTEGER, date TEXT, amount DOUBLE)');
    });
  } catch (err) {
      console.log("Error creating table in database: " + err);
  }

  return database;
}

function readAllSavings()
{
  var database = openDatabase();
  var result = null;

  try {
    database.transaction(function (tx) {
        result = tx.executeSql(
                    'SELECT * FROM savings')
    })
  } catch (err) {
      console.log("Error reading from database: " + err);
  }

  return result;
}

function readLastSavingId() {
  var database = openDatabase();
  var result = null;

  try {
    database.transaction(function (tx) {
        result = tx.executeSql(
                    'SELECT * FROM savings ORDER BY id DESC LIMIT 1')
    })
  } catch (err) {
      console.log("Error reading from database: " + err);
  }

  return result.rows.item(0).id;
}

function readSaving(id)
{
  var database = openDatabase();
  var result = null;

  try {
    database.transaction(function (tx) {
        result = tx.executeSql(
                    'SELECT * FROM savings WHERE id == ' + id)
    })
  } catch (err) {
      console.log("Error reading from database: " + err);
  }

  return result
}

function readSavingHistory(id) {
  var database = openDatabase();
  var results = null;

  try {
    database.transaction(function (tx) {
        results = tx.executeSql(
                    'SELECT date, amount FROM savingsHistory WHERE savings_id == ? ORDER BY id DESC', [id])
    })
  } catch (err) {
      console.log("Error reading from database: " + err);
  }

  return results;
}

function writeSaving(title, amount) {
  var database = openDatabase();

  try {
    database.transaction(function (tx) {
        tx.executeSql('INSERT INTO savings (title, notes, url, amount) VALUES (?, ?, ?, ?)', [title, "", "", amount])
    })
  } catch (err) {
      console.log("Error writing to database: " + err);
  }
}

function writeSavingHistory(id, amount) {
  var database = openDatabase();
  let currentDate = new Date();

  var dateString = currentDate.toLocaleDateString(Qt.locale(),Locale.ShortFormat) + " " + currentDate.toLocaleTimeString(Qt.locale(),Locale.ShortFormat)

  try {
    database.transaction(function (tx) {
        tx.executeSql('INSERT INTO savingsHistory (savings_id, date, amount) VALUES (?, ?, ?)', [id, dateString, amount])
    })
  } catch (err) {
      console.log("Error writing to database: " + err);
  }
}

function deleteSaving(id) {
  var database = openDatabase();

  try {
    database.transaction(function (tx) {
        tx.executeSql('DELETE FROM savings WHERE id == ?;', [id])
        tx.executeSql('DELETE FROM savingsHistory WHERE savings_id == ?', [id])
    })
  } catch (err) {
      console.log("Error writing to database: " + err);
  }
}

function updateSaving(id, amount, url, notes) {
  var database = openDatabase();

  try {
    database.transaction(function (tx) {
        tx.executeSql('UPDATE savings SET notes = ?, url = ?, amount = ? WHERE id == ?;', [notes, url, amount, id])
    })
  } catch (err) {
      console.log("Error updating the database: " + err);
  }
}
